// / JavaScript Bitwise Operators
// / & AND(not to be confused with &&) Sets each bit to 1 if both bits are 1
// / |	OR	Sets each bit to 1 if one of two bits is 1
// / ^	XOR	Sets each bit to 1 if only one of two bits is 1
// / ~	NOT	Inverts all the bits
// / <<	Zero fill left shift	Shifts left by pushing zeros in from the right and let the leftmost bits fall off
// / >>	Signed right shift	Shifts right by pushing copies of the leftmost bit in from the left, and let the rightmost bits fall off
// / >>> Zero fill right shift	Shifts right by pushing zeros in from the left, and let the rightmost bits fall off

console.log((3).toString(2)); ///return 3's quivalent binary 11
console.log((5).toString(2)); ///return 5's quivalent binary 101

// /Binary to decimal
console.log((0b11101).toString(10)); ///returns 29

// /Hoisting is JavaScript's default behavior of
// /moving declarations to the top.

one = 5;
console.log(one);
var one;
// /Under the hood
// /this happens
// /var x=undefined
// /x=5
/// console.log(x);

// /but for let and const this thing
// /doens't happen completely
// /let x=nothing initialized

// /JavaScript Initializations are Not Hoisted
/// JavaScript only hoists declarations, not initializations.

var x = 5; // Initialize x
var y = 7; // Initialize y

console.log((sum = x + " " + y)); // / 5  7

var p = 5; // Initialize x

console.log(
  (anotherSum = p + " " + q) // / 5   undefined
);

var q = 7; // Initialize y

// /    The let and const Keywords
// /Using a let variable before it is
// /declared will result in a ReferenceError.

// /Using a const variable before it is declared,
// /is a syntax error, so the code will simply not run.

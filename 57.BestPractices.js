// /          Avoid global variables, avoid new, avoid ==, avoid eval()

// Minimize the use of global variables.

// This includes all data types,
// objects, and functions.

// Global variables and functions
// can be overwritten by other scripts.

// /          Always Declare Local Variables
// All variables used in a function
// should be declared as local variables.

// Local variables must be declared with the var,
// the let, or the const keyword,
// otherwise they will become global variables.

// /          Declarations on Top
// It is a good coding practice to put all
// declarations at the top of each script or function.

// This will:

// Give cleaner code
// Provide a single place to look for local variables
// Make it easier to avoid unwanted (implied) global variables
// Reduce the possibility of unwanted re-declarations

// Declare at the beginning
let firstName, lastName, price, discount, fullPrice;

// Use later
firstName = "John";
lastName = "Doe";

price = 19.9;
discount = 0.1;

fullPrice = price - discount;

// /         Or

// Declare and initiate at the beginning
let firstName = "";
let lastName = "";
let price = 0;
let discount = 0;
let fullPrice = 0,
const myArray = [];
const myObject = {};


// /          Declare Objects with const
// Declaring objects with const will prevent 
// any accidental change of type

let car = {type:"Fiat", model:"500", color:"white"};
car = "Fiat";      // Changes object to string

const car = {type:"Fiat", model:"500", color:"white"};
car = "Fiat";      // Not possible


// /Don't Use new Object()
// Use "" instead of new String()
// Use 0 instead of new Number()
// Use false instead of new Boolean()
// etc...


let x1 = "";             // new primitive string
let x2 = 0;              // new primitive number
let x3 = false;          // new primitive boolean
const x4 = {};           // new object
const x5 = [];           // new array object
const x6 = /()/;         // new regexp object
const x7 = function(){}; // new function object


// /Beware of Automatic Type Conversions
// JavaScript is loosely typed.

// A variable can contain all data types.

// A variable can change its data type

// Example
let x = "Hello";     // typeof x is a string
x = 5;               // changes typeof x to a number

// Beware that numbers can accidentally be 
// converted to strings or NaN (Not a Number).

// When doing mathematical operations, 
// JavaScript can convert numbers to strings


let x = 5 + 7;       // x.valueOf() is 12,  typeof x is a number
let x = 5 + "7";     // x.valueOf() is 57,  typeof x is a string
let x = "5" + 7;     // x.valueOf() is 57,  typeof x is a string
let x = 5 - 7;       // x.valueOf() is -2,  typeof x is a number
let x = 5 - "7";     // x.valueOf() is -2,  typeof x is a number
let x = "5" - 7;     // x.valueOf() is -2,  typeof x is a number
let x = 5 - "x";     // x.valueOf() is NaN, typeof x is a number


// Subtracting a string from a string, 
// does not generate an error but returns NaN (Not a Number):

Example
"Hello" - "Dolly"    // returns NaN


// / Use === Comparison
// The == comparison operator always converts 
// (to matching types) before comparison.

// The === operator forces comparison of values and type:

Example
0 == "";        // true
1 == "1";       // true
1 == true;      // true

0 === "";       // false
1 === "1";      // false
1 === true;     // false


// / Use Parameter Defaults
// If a function is called with a missing argument, 
// the value of the missing argument is set to undefined.

// Undefined values can break your code. 
// It is a good habit to assign default values to arguments.

// Example
function myFunction(x, y) {
  if (y === undefined) {
    y = 0;
  }
}


// Do this 

function useDefaultParmas (a=1, b=1) { /*function code*/ }


// / End Your Switches with Defaults
// Always end your switch statements with a default. 
// Even if you think there is no need for it.
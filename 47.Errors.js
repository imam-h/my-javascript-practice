/// The try statement defines a code block to run (to try).

/// The catch statement defines a code block to handle any error.

/// The finally statement defines a code block to run regardless of the result.

/// The throw statement defines a custom error.

try {
  addlert("we;come");
} catch (error) {
  console.log(error.message);
  // / executed this block of code inside catch
  /// because no addlert function available in javascript
}

// * throw section
try {
  if (x.trim() == "") throw "empty";
  if (isNaN(x)) throw "not a number";
  x = Number(x);
  if (x < 5) throw "too low";
  if (x > 10) throw "too high";
} catch (err) {
  message = "Input is " + err;
}

/// When an error occurs,
// /JavaScript will normally stop and generate an error message.

/// The technical term for this is:
// /JavaScript will throw an exception (throw an error)

// try {
// } catch (error) {
// } finally {
// / Block of code to be executed regardless
// / of the try / catch result
// }

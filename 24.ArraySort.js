const fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.sort());
console.log(fruits.reverse());

// /By combining sort() and reverse(),
// /we can sort an array in descending order
// /ES2023 added the toSorted(),toReversed() method as
// /safe way to sort an array without altering the original array.

const points = [40, 100, 40, 1, 5, 25, 10];

const ascending = points.sort(function (a, b) {
  return a - b;
});

const descending = points.sort(function (a, b) {
  return b - a;
});
console.log(ascending);
console.log(descending);
console.log(points);

// /By default, the sort() function sorts values as strings.
// /If numbers are sorted as strings, "25" is bigger than "100",
// /because "2" is bigger than "1".
// /we can fix this by providing a compare function
// /If the result is negative, a is sorted before b.
/// If the result is positive, b is sorted before a.
/// If the result is 0, no changes are done
// /with the sort order of the two values.

//! Sorting Object Arrays

const cars = [
  { type: "Volvo", year: 2016 },
  { type: "Saab", year: 2001 },
  { type: "BMW", year: 2010 },
];

let carsSorted = cars.sort(function (a, b) {
  return a.year - b.year;
});

cars.sort(function (a, b) {
  let x = a.type.toLowerCase();
  let y = b.type.toLowerCase();
  if (x < y) {
    return -1;
  }
  if (x > y) {
    return 1;
  }
  return 0;
});

console.log(carsSorted);

// /Comparing string properties is a little more complex
// ECMAScript 2015, also known as ES6,
// introduced JavaScript Classes.

// JavaScript Classes are templates for
// JavaScript Objects.

// JavaScript Class Syntax
// Use the keyword class to create a class.

class Car {
  /// Always add a method named constructor()
  constructor(name, year) {
    this.theName = name; /// this refers to the object
    this.theYear = year;
  }

  // /Add method
  drive() {
    console.log(`${this.theName} is being driven`);
  }

  // / Add another method
  duration(time) {
    console.log(`${this.theName} is being driven for ${time}`);
  }
}
const firstCar = new Car("BMW", 1990); /// created a class instance
const secondCar = new Car("Mazda", 1991); /// created a class instance

console.log(firstCar.theName);
secondCar.drive();
firstCar.duration("3 hours");

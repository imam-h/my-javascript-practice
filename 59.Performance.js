// / Reduce Activity in Loops

// Bad:
for (let i = 0; i < arr.length; i++) {}

// Better Code:
let l = arr.length;
for (let i = 0; i < l; i++) {}

// The bad code accesses the length property of
// an array each time the loop is iterated.

// The better code accesses the length property
// outside the loop and makes the loop run faster.

// / Reduce DOM Access
// Accessing the HTML DOM is very slow,
// compared to other JavaScript statements.

// If you expect to access a DOM element several times,
// access it once, and use it as a local variable:

// Example

// stored in a variable
// const obj = document.getElementById("demo");
// obj.innerHTML = "Hello";

// / Avoid Unnecessary Variables
// Don't create new variables
// if you don't plan to save values.

// Often you can replace code like this:

// let fullName = firstName + " " + lastName;
// document.getElementById("demo").innerHTML = fullName;

// With this:

// document.getElementById("demo").innerHTML = firstName + " " + lastName;

// / Delay JavaScript Loading
// Putting your scripts at the bottom of the
// page body lets the browser load the page first.

// While a script is downloading,
// the browser will not start any other downloads.
// In addition all parsing and rendering activity might be blocked.
// An alternative is to use defer="true" in the script tag.
// The defer attribute specifies that the
// script should be executed after
// the page has finished parsing,
// but it only works for external scripts.

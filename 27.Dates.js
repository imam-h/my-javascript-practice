const d = new Date(2018, 6, 5, 10, 33, 30);
console.log(d);
// /Good result on browser
// /Date objects are created with the new Date() constructor.
// /There are 9 ways to create a new date object:

// /new Date()
// /new Date(date string)

// /new Date(year,month)
// /new Date(year,month,day)
// /new Date(year,month,day,hours)
// /new Date(year,month,day,hours,minutes)
// /new Date(year,month,day,hours,minutes,seconds)
// /new Date(year,month,day,hours,minutes,seconds,ms)

// /new Date(milliseconds)

const now = new Date();
console.log(now.toUTCString());
console.log(now.toDateString());
console.log(now.toISOString());
// /works ideally in node
// /except toISOString()

// /Basic comaprisons operators like in math
// /extra to remember
// / ===	equal value and equal type
// / !==	not equal value or not equal type
// / >=	greater than or equal to
// / <=	less than or equal to

// / Logical Operators are
// / &&	and
// / ||	or
// / !  not

// / Conditional (Ternary) Operator
// / let voteable = (age < 18) ? "Too young":"Old enough";

// /"2" < "12"	// false because 1 is less than 2.
// /To secure a proper result,
// /variables should be converted to the proper type before comparison

// /The ?? operator returns the first argument
// /if it is not nullish (null or undefined).

// /Otherwise it returns the second argument.

// /Example
let nameOne = null;
let text = "missing";
let result = nameOne ?? text; // result = "missing"
console.log(result);

// /The ?. operator returns undefined if an object is undefined or null
// /(instead of throwing an error)

// /Example
const car = { type: "Fiat", model: "500", color: "white" };
// Ask for car name:
console.log(car?.name);

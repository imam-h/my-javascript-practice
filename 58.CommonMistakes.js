// / Accidentally Using the Assignment Operator
// JavaScript programs may generate unexpected results
// if a programmer accidentally uses an assignment operator (=),
// instead of a comparison operator (==) in an if statement.

// / Expecting Loose Comparison
// In regular comparison,
// data type does not matter.
// This if statement returns true
// It is a common mistake to forget that
// switch statements use strict comparison

// / Confusing Addition & Concatenation
// Addition is about adding numbers.

// Concatenation is about adding strings.

// In JavaScript both operations use the same + operator.

let x = 10;
x = 10 + 5; // Now x is 15

let y = 10;
y += "5"; // Now y is "105"

let x = 10;
let y = 5;
let z = x + y; // Now z is 15

let x = 10;
let y = "5";
let z = x + y; // Now z is "105"

// / Misunderstanding Floats
// All numbers in JavaScript are stored
// as 64-bits Floating point numbers (Floats).


let x = 0.1;
let y = 0.2;
let z = x + y  // the result in z will not be 0.3


// To solve the problem above, 
// it helps to multiply and divide:

Example
let z = (x * 10 + y * 10) / 10;    

// / Breaking a JavaScript String

let j =
"Hello World!"; // OK


let xy = "Hello
World!"; // / error


// To solve, use backslash
let xz = "Hello \
World!";



// / Misplacing Semicolon
// Because of a misplaced semicolon, 
// this code block will execute regardless of the value of x:

if (x == 19);
{
  // code block 
}


// / Breaking a Return Statement


function myFunction(a) {
    let
    power = 10; 
    return a * power;

    // not like this
//     return
//   a * power;
  }

//   Never break a return statement.


// / Accessing Arrays with Named Indexes
// Many programming languages support arrays with named indexes.

// Arrays with named indexes are called associative arrays (or hashes).

// JavaScript does not support arrays with named indexes.

// In JavaScript, arrays use numbered indexes 


const person = [];
person["h"] = "John"; // hash, not supported in JS arrays
person[1] = "Doe";
person[2] = 46;
person.length;       // person.length will return 3
person[0]; 


// If you use a named index, when accessing an array, 
// JavaScript will redefine the array to a standard object.

// After the automatic redefinition, 
// array methods and properties will produce 
// undefined or incorrect results
// In JavaScript, objects use named indexes.


// / Ending Definitions with a Comma
const person = {firstName:"John", lastName:"Doe", age:46,} //not OK
points = [40, 100, 1, 5, 25, 10,]; // not OKAY

// because
// Internet Explorer 8 will crash.

// JSON does not allow trailing commas.


//  / Undefined is Not Null
// JavaScript objects, variables, properties, and methods can be undefined.

// In addition, empty JavaScript objects can have the value null.

// This can make it a little bit difficult to test if an object is empty.

// You can test if an object exists by testing if the type is undefined


Incorrect:
if (myObj !== null && typeof myObj !== "undefined") 


    Correct:
if (typeof myObj !== "undefined" && myObj !== null) 
/// Everything With a "Value" is True
/// Everything Without a "Value" is False

console.log(10 > 9); // returns true

let x = 0;
console.log(Boolean(x)); // returns false

// /same for
// /let x = -0;
// /let x = "";
// /let x; here x is defined
// /let x = null;
// /let x = false;
// /let x = 10 / "Hallo"; Nan
// /These are called "falsy values"

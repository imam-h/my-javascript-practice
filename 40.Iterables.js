// /Iterables are iterable objects (like Arrays).
// /Iterables can be iterated over with for..of loops

// /Iterating over a string
const name = "W3Schools";

for (const x of name) {
  console.log(x);
}

// /Iterating over a array

const letters = ["a", "b", "c"];

for (const x of letters) {
  console.log(x);
}

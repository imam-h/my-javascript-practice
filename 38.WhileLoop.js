// *Simple while loop

let i = 0;
let j = 10;

while (i < 10) {
  console.log(i);
  i++;
}

// /If you forget to increase the variable used in
// /the condition, the loop will never end.
// /This will crash your browser.
// /-----------------------------
// /The do while loop is a variant of the while loop.
// /This loop will execute the code block at least once,
// /before checking if the condition is true,
// /then it will repeat the loop as long as the condition is true
do {
  console.log(j);

  j++;
} while (j < 10);

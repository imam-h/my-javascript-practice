// /        Variable Names
// At W3schools we use camelCase for
// identifier names (variables and functions).
// All names start with a letter.
// Example
// let firstName = "John";
// price = 19.90;

// /        spaces Around Operators
// Always put spaces around operators ( = + - * / ),
// and after commas

// /        Code Indentation
// Always use 2 spaces for indentation of code blocks

// /        Statement Rules
// Always end a simple statement with a semicolon.

// /        General rules for complex (compound) statements:

// Put the opening bracket at the end of the first line.
// Use one space before the opening bracket.
// Put the closing bracket on a new line, without leading spaces.
// Do not end a complex statement with a semicolon.

// /        General rules for object definitions:

// Place the opening bracket on the
// same line as the object name.
// Use colon plus one space between
// each property and its value.
// Use quotes around string values,
// not around numeric values.
// Do not add a comma after the last
// property-value pair.
// Place the closing bracket on a new
// line, without leading spaces.
// Always end an object definition with a semicolon.

// /        Line Length < 80
// For readability, avoid lines longer than 80 characters.
// If a JavaScript statement does not fit on one line,
// the best place to break it, is after an operator or a comma.

// Example;
// document.getElementById("demo").innerHTML = "Hello Dolly.";

// /        Naming Conventions
// Always use the same naming convention for all your code.
// For example:
// Variable and function names written as camelCase
// Global variables written in UPPERCASE (We don't, but it's quite common)
// Constants (like PI) written in UPPERCASE
// Hyphens can be mistaken as subtraction attempts.
// Hyphens are not allowed in JavaScript names.
// Do not start names with a $ sign.
// It will put you in conflict with many JavaScript library names.

// /        Use Lower Case File Names
// Most web servers (Apache, Unix) are
// case sensitive about file names:

// london.jpg cannot be accessed as London.jpg.

// Other web servers (Microsoft, IIS) are not case sensitive:

// london.jpg can be accessed as London.jpg or london.jpg.

// If you use a mix of upper and lower case,
// you have to be extremely consistent.

// If you move from a case insensitive,
// to a case sensitive server, even small errors can break your web site.

// To avoid these problems, always use lower case file names (if possible).

// /        Performance
// Coding conventions are not used by computers.

// Most rules have little impact on the execution of programs.

// Indentation and extra spaces
// are not significant in small scripts.

// For code in development, readability should be preferred.
// Larger production scripts should be minimized.

// /JavaScript variables can be converted to a
// /new variable and another data type:

// /1..By the use of a JavaScript function
// /2..Automatically by JavaScript itself

// /Converting Strings to Numbers
// /The global method Number()
// /converts a variable (or a value) into a number.

// !These will convert:

Number("3.14"); /// 3.14
Number(Math.PI); /// 3.1415..
Number(" "); /// 0
Number(""); ///0

// !These will not convert
Number("99 88"); /// NaN
Number("John"); /// NaN

// /Number other methods
// /parseFloat()	Parses a string and returns a floating point number
// /parseFloat('3.13') // returns 3.13
// /parseInt()	Parses a string and returns an integer
// /parseInt('3.5') // returns 3

/// The Unary (+) Operator
let y = "5"; // y is a string
let x = +y; // x is a number

//  but for
// let y = "John"; // y is a string
// let x = +y; // x is a number but NaN

/// Converting Numbers to Strings
String(123); /// returns a string from a number literal 123
String(100 + 23); /// returns a string from a number from an expression

// !The Number method toString() does the same.

// /Converting Booleans to Numbers

Number(false); // returns 0
Number(true); // returns 1

/// Converting Booleans to Strings
String(false); // returns "false"
String(true); // returns "true"
/// The Boolean method toString() does the same.

// !Automatic Type Conversion
// /When JavaScript tries to operate on a "wrong"
// /data type, it will try to convert the value to a "right" type.

// ?The result is not always what you expect

5 + null; // returns 5         because null is converted to 0
"5" + null; // returns "5null"   because null is converted to "null"
"5" + 2; // returns "52"      because 2 is converted to "2"
"5" - 2; // returns 3         because "5" is converted to 5
"5" * "2"; // returns 10        because "5" and "2" are converted to 5 and 2

// /Automatic String Conversion

let myVar = { name: "Fjohn" }; // toString converts to "[object Object]"
// let  myVar = [1,2,3,4]       // toString converts to "1,2,3,4"
// let myVar = new Date()      // toString converts to "Fri Jul 18 2014 09:08:55 GMT+0200"

// / Check JavaScript Type Conversion Table
// /from w3school

const variables = ["let", "const", "var"];
/// variables = ["strong"]; // error
// /const named as varaibles can't be assigned to another value
// /But we can add/delete/replace values from it
// /It's like a referrence where we refer to
// /Important point is that
// /const must be assigned when declared
// /But using var in array can be initialized at any time
// /even we can use it before it is declared
// /An array declared with const has Block Scope
// /An array declared with var does not have block scope
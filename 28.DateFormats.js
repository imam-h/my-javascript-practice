// /There are generally 3 types of JavaScript date input formats:

// /ISO Date	"2015-03-25" (The International Standard)
// /Short Date	"03/25/2015"
// /Long Date	"Mar 25 2015" or "25 Mar 2015"

const d = new Date("2015-03-25");
console.log(d);

const now = Date.parse("10 Aug 2024");
const nowTime = new Date(now);
console.log(nowTime);

// /UTC (Universal Time Coordinated) is the same as GMT (Greenwich Mean Time).

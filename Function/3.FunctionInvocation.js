// / Invoking a JavaScript Function

// The code inside a function is not executed
// when the function is defined.

// The code inside a function is executed when
// the function is invoked.

// It is common to use the term "call a function"
// nstead of "invoke a function".

// It is also common to say "call upon a function",
// "start a function", or "execute a function".

// In this tutorial, we will use invoke,
// because a JavaScript function can be invoked without being called.

// / Invoking a Function as a Function
function myFunction(a, b) {
  return a * b;
}

console.log(myFunction(10, 2));
// console.log(window.myFunction(10, 2)); //try in browser,result same as above

// The function above does not belong to any object.
// But in JavaScript there is always a default global object.

// In HTML the default global object is the HTML page itself,
// so the function above "belongs" to the HTML page.

// In a browser the page object is the browser window.
// The function above automatically becomes a window function.

// / myFunction() and window.myFunction() is the same function

// / What is this?
// In JavaScript, the this keyword refers to an object.

// The this keyword refers to different
// objects depending on how it is used:

// In an object method, this refers to the object.
const This = {
  a: function () {
    console.log(this);
  },
};
console.log(This.a);

// Alone, this refers to the global object.
// In a function, this refers to the global object.
// In a function, in strict mode, this is undefined.
// In an event, this refers to
// the element that received the event.
// Methods like call(), apply(),
// and bind() can refer this to any object.

// / The Global Object
// When a function is called without an owner object,
//  the value of this becomes the global object.

// In a web browser the global object
// is the browser window.

// This example returns the window
// object as the value of this:

let x = myFunction(); // x will be the window object

function myFunction() {
  return this;
}

console.log(x);

// Invoking a Function as a Method
// In JavaScript you can define

// functions as object methods.

// The following example creates an
// object (myObject), with two properties (firstName and lastName),
// and a method (fullName):

const myObject = {
  firstName: "John",
  lastName: "Doe",
  fullName: function () {
    return this.firstName + " " + this.lastName;
  },
};
myObject.fullName(); // called as object method(known as invoke)

// Invoking a Function with a Function Constructor
// If a function invocation is preceded with
// the new keyword, it is a constructor invocation.

// It looks like you create a new function,
// but since JavaScript functions are objects you actually create a new object:

// This is a function constructor:
function myFunction(arg1, arg2) {
  this.firstName = arg1;
  this.lastName = arg2;
}

// This creates a new object
const myObj = new myFunction("John", "Doe");

// This will return "John"
myObj.firstName;

// / In short , function call means direct call like myFunc()
// / invoke means calling via object like myObj.myFunc()

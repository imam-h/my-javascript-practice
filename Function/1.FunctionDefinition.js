// / JavaScript functions are defined
// / with the function keyword.

// / You can use a function declaration
// / or a function expression.

// / Function Declarations

function functionName(parameters) {
  // code to be executed
}
// Declared functions are not executed immediately.
// They are "saved for later use", and will be executed later
// when they are invoked (called upon).

function myFunct(a, b) {
  return a * b;
}

console.log(myFunct(1, 2)); // called or invoked here

// / Function Expressions
// A JavaScript function can also be
// defined using an expression.
// A function expression can be
// stored in a variable:

const x = function (a, b) {
  return a * b;
}; // last semicolon because it's a expression

let z = x(2, 3);
console.log(z);

// The function above is actually an
// anonymous function (a function without a name).

// Functions stored in variables do not need
// function names. They are always invoked (called)
// using the variable name

// / Function Hoisting
// Hoisting is JavaScript's default behavior
// of moving declarations to the top of the current scope.

// Hoisting applies to variable declarations and
// to function declarations.

// Because of this, JavaScript functions can be
// called before they are declared:

myFunc(5);

function myFunc(y) {
  return y * y;
}

// !!!! Functions defined using an
// !!!! expression are not hoisted.

// / Self-Invoking Functions
// Function expressions can be made "self-invoking".

// A self-invoking expression is invoked
// (started) automatically, without being called.

// Function expressions will execute automatically
// if the expression is followed by ().

// You cannot self-invoke a function declaration.

// You have to add parentheses around the function
// to indicate that it is a function expression:

(function () {
  let x = "Hello!!"; // I will invoke myself
  console.log(x);
})();

// / Functions Can Be Used as Values
// JavaScript functions can be used as values
function myFunct(a, b) {
  return a * b;
}

let value = myFunct(4, 3);
console.log(value);

// / JavaScript functions can be used in expressions

// Example;
function myFunction(a, b) {
  return a * b;
}

let express = myFunction(4, 3) * 2;

// / Functions are Objects
// The typeof operator in JavaScript
// returns "function" for functions.

// But, JavaScript functions can best
// be described as objects.

// JavaScript functions have both
// properties and methods.

// The arguments.length property returns
// the number of arguments received when the function was invoked

function myFunctionObj(a, b) {
  return arguments.length;
}

console.log(myFunctionObj(1, 2)); // 2

/*//!A function defined as the property of an object, is called a method to the object.
//! A function designed to create new objects, is called an object constructor.*/

// / Arrow Functions
// Arrow functions allows a short syntax
// for writing function expressions.

// You don't need the function keyword,
// the return keyword, and the curly brackets.

// ES5
var ES5 = function (x, y) {
  return x * y;
};

// ES6
const ES6 = (x, y) => x * y;

// Arrow functions do not have their own this.
// They are not well suited for defining object methods.

// Arrow functions are not hoisted.
// They must be defined before they are used.

// Using const is safer than using var,
// because a function expression is always constant value.

// You can only omit the return keyword and
// the curly brackets if the function is a single statement.
// Because of this, it might be a good habit to always keep them

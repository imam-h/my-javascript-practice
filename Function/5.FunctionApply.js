// / Method Reuse
// With the apply() method, you can write a method
// that can be used on different objects.

// / The JavaScript apply() Method
// The apply() method is similar to the call() method

// / The Difference Between call() and apply()
// The difference is:

// The call() method takes arguments separately.

// The apply() method takes arguments as an array.

const person = {
  fullName: function (city, country) {
    return this.firstName + " " + this.lastName + "," + city + "," + country;
  },
};

const person1 = {
  firstName: "John",
  lastName: "Doe",
};

console.log(person.fullName.apply(person1, ["Oslo", "Norway"]));

// / Simulate a Max Method on Arrays
// You can find the largest number (in a list of numbers)
// using the Math.max() method

console.log(Math.max.apply(null, [1, 2, 3]));

// The first argument (null) does not matter.
// It is not used in this example.

// These examples will give the same result:

// Math.max.apply(Math, [1,2,3]); // Will also return 3

// Math.max.apply(" ", [1,2,3]); // Will also return 3

// Math.max.apply(0, [1,2,3]);

// / JavaScript Strict Mode
// In JavaScript strict mode, if the first argument of the
// apply() method is not an object,
// it becomes the owner (object) of the
// invoked function. In "non-strict" mode,
// it becomes the global object.

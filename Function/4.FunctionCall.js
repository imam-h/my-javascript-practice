// / Method Reuse
// With the call() method,
// you can write a method that
// can be used on different objects.

// All Functions are Methods
// In JavaScript all functions are object methods.

// If a function is not a method of a JavaScript object,
// it is a function of the global object

// / The JavaScript call() Method
// The call() method is a predefined JavaScript method.

// It can be used to invoke (call) a method
// with an owner object as an argument (parameter).

// With call(), an object can use a method
// belonging to another object.
const person = {
  fullName: function () {
    return this.firstName + " " + this.lastName;
  },
};
const person1 = {
  firstName: "John",
  lastName: "Doe",
};
const person2 = {
  firstName: "Mary",
  lastName: "Doe",
};

// This will return "John Doe":
console.log(person.fullName.call(person1));

// / The call() Method with Arguments

const withArg = {
  fullName: function (city, country) {
    return this.firstName + " " + this.lastName + "," + city + "," + country;
  },
};

const demo = {
  firstName: "mary",
  lastName: "Doe",
};

console.log(withArg.fullName.call(demo, "Oslo", "Norway"));

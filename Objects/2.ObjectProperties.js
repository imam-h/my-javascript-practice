// Now, you can see this topic in w3school
// under JS objects in 4th serial
// But at the time of the video
// it was in 2nd serial

// / JavaScript Object.defineProperty()
// Object.defineProperty() method can be used to:

// Adding a new property to an object
// Changing property values
// Changing property metadata
// Changing object getters and setters

// Syntax:

// Object.defineProperty(targetObjectName, property, descriptor)

// This example adds a new property to an object:

// Create an Object:
const person = {
  firstName: "John",
  lastName: "Doe",
  language: "EN",
};

// Add a Property
Object.defineProperty(person, "year", {
  value: "2008",
  // to add it to the person object
  enumerable: true,
  // writable: false,
  // configurable: true,
});

// This example makes language read-only:
Object.defineProperty(person, "language", { configurable: false });

// Change a Property
Object.defineProperty(person, "language", { value: "NO" });

console.log(person.language);

// ! Read ! very important
// All properties have a name.
// In addition they also have a value.
// The value is one of the property's attributes.
// Other attributes are: enumerable, configurable, and writable.

// These attributes define how the
// property can be accessed (is it readable?, is it writable?)
// In JavaScript, all attributes can be read,
// but only the value attribute can be
// changed, not the name (and only if the property is writable).

// / JavaScript getOwnPropertyNames()

// Object.getOwnPropertyNames() method can:
// List object properties, all of
// Object.getOwnPropertyNames()
// will also list properties that is not enumerable

console.log(Object.getOwnPropertyNames(person));

// / Object.keys()
// Object.keys() method can
// List //! enumerable object properties

// Add another Property to person
Object.defineProperty(person, "job", {
  value: "Teacher",
  // Object.keys(person) ignores the name for this
  enumerable: false,
});

console.log(Object.keys(person)); // return all except 'job'
console.log(Object.getOwnPropertyNames(person)); // return all name

// / Adding Getters and Setters
// .....

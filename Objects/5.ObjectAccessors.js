// / JavaScript Accessors (Getters and Setters)
// ECMAScript 5 (ES5 2009) introduced Getter and Setters.

// Getters and setters allow you to define
// Object Accessors (Computed Properties).
// Computed Properties means new property computed from
// existing property

// / JavaScript Getter (The get Keyword)
// This example uses a lang property to get
// the value of the language property.

// Create an object:
const person = {
  firstName: "John",
  lastName: "Doe",
  language: "en",
  //   get keyword
  get lang() {
    // return this.language;
    return this.firstName + " " + this.lastName;
  },
};

console.log(person.lang); // not person.lang(), it is getter

// /JavaScript Setter (The set Keyword)
// This example uses a lang property to
// set the value of the language property.

const anotherPerson = {
  firstName: "John",
  lastName: "Doe",
  language: "",
  set lang(lang) {
    this.language = lang;
  },
};

// Set an object property using a setter:
anotherPerson.lang = "en";

console.log(anotherPerson);

// ?  JavaScript Function or Getter
// ? Why Using Getters and Setters?
// It gives simpler syntax
// It allows equal syntax for properties and methods
// It can secure better data quality
// It is useful for doing things behind-the-scenes

// / Object.defineProperty()
// The Object.defineProperty() method can
// also be used to add Getters and Setters
// just like we add property to any object later

// Define object
const obj = { counter: 0, name: "Demo", showAs: "example" };

// Add one getter
Object.defineProperty(obj, "reset", {
  get: function () {
    return (this.counter = 0);
  },
});

console.log(obj.reset);

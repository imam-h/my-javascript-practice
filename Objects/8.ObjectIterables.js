// The iterator protocol defines how to
// produce a sequence of values from an object.

// An object becomes an iterator when
// it implements a next() method.

// The next() method must return an
// object with two properties:

// --- value (the next value)
// --- done (true or false)

// value is the value returned by the iterator
// (Can be omitted if done is true)

// done is true if the iterator has completed
// false if the iterator has produced a new value

// Technically, iterables must implement the
// Symbol.iterator method.
// when we use for of loop in a object
// we get error saying ... is not a iterable

// String, Array, TypedArray, Map and Set are all iterables
// because their prototype objects have a
// Symbol.iterator method.

// const numbers = { 1: "one", 2: "we", 3: "gh" };
const numbers = [1, 2, 3];

const numIterator = numbers[Symbol.iterator](); //Array iterator object

console.log(numIterator); //Array iterator object

console.log(numIterator.next()); // {value:1, done:false}
console.log(numIterator.next()); // {value:2, done:false}
console.log(numIterator.next()); // {value:3, done:false}

console.log(numIterator.next()); // {value:undefined, done:true}

// / How to make a object iterable
// return value and done as an object from next function

const makeItIterable = {
  //   one: 1,
  //   two: 2,
  //   three: 3,
};
console.log(typeof Symbol.iterator); // symbol :)

makeItIterable[Symbol.iterator] = function () {
  let done = false;
  let n = 0;

  return {
    next() {
      n += 10;
      if (n == 10) {
        done = true;
      }
      return {
        value: n,
        done: done,
      };
    },
  };

  for (let x of makeItIterable) {
    return x;
  }
};

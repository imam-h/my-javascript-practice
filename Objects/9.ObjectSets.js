// A JavaScript Set is a collection of unique values.

// ! Each value can only occur once in a Set.

// The values can be of any type, primitive values or objects.

// / How to Create a Set
// You can create a JavaScript Set by:
// Passing an Array to new Set()
// Create a Set and use add() to add values

const letters = new Set(["a", "b", "c"]);

// Or create a empty Set
// then add using add method

// / The add() method inserts a new element in the set.
letters.add("d");

console.log(letters);

// / values()
// The values() method returns an Iterator
// object with the values in a set.
// The values() method does not change the original set.
const myIterator = letters.values();
for (const entry of myIterator) {
  console.log(entry);
}

// / keys()
// Since a set has no keys, the keys()
// method returns the same as values().
// This makes JavaScript sets compatible with JavaScript maps.
console.log(letters.keys());

// / entries ()
// The entries() method returns an Iterator
// with [value,value] pairs from a set.
// Since a set has no keys, the entries()
// method returns [value,value].
const myIteratorforEntry = letters.entries();
for (let entry of myIteratorforEntry) {
  console.log(entry);
}

// / instanceof
// Set is the typeof object
// use instanceof to determine whether it is set
console.log(letters instanceof Set);

// / The differences between map and object
// A Map holds key-value pairs where
// the keys can be any datatype.
// A Map remembers the original
// insertion order of the keys.

// Create a Map
const fruits = new Map([
  ["apples", 500],
  ["bananas", 300],
  ["oranges", 200],
]);

// Or
// Create a Map
const fruitss = new Map();

// Set Map Values
fruitss.set("apples", 500);
fruitss.set("bananas", 300);
fruitss.set("oranges", 200);

// / The set() method can also be
// / used to change existing Map values
// fruits.set("apples", 500);

// / Visit https://www.w3schools.com/jsref/jsref_obj_map.asp

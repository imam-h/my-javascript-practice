// ! you don't see the subject explicitly
// ! in w3school right now
// ! but remains in video

// Sometimes we need to create
// many objects of the same type.

// To create an object type we use
// an object constructor function.

// It is considered good practice to name
// constructor functions with an upper-case first letter.

function Person(first, last, age, eye) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.eyeColor = eye;
  this.fullName = function () {
    return this.firstName + " " + this.lastName;
  };
}

const mySelf = new Person("Johnny", "Rally", 22, "green");
const mySister = new Person("Anna", "Rally", 18, "green");

console.log(mySelf);
console.log(mySelf.fullName()); // returns 'Johnny Rally'

console.log(mySister);

// Adding a property to an object
mySelf.occupation = "unemployed";
console.log(mySelf);

// Similarly we can add method to an object
// ................

// Adding a property to the constructor function
// not allowed

// / Built-in JavaScript Constructors
// JavaScript has built-in
// constructors for all native objects:

new Object(); // A new Object object
new Array(); // A new Array object
new Map(); // A new Map object
new Set(); // A new Set object
new Date(); // A new Date object
new RegExp(); // A new RegExp object
new Function(); // A new Function object

// ! Note:
// The Math() object is not in the list. Math is a global object.
// The new keyword cannot be used on Math.

// ! Always
// Use object literals {} instead of new Object().

// Use array literals [] instead of new Array().

// Use pattern literals /()/ instead of new RegExp().

// Use function expressions () {} instead of new Function().

// "";           // primitive string
// 0;            // primitive number
// false;        // primitive boolean

// {};           // object object
// [];           // array object
// /()/          // regexp object
// function(){}; // function

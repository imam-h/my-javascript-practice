// ! you don't see the subject
// ! in w3school right now
// ! but remains in video

// / In browser if you want to log
// a object to display
// you should log one value at once
// because console.log(objectName) shows [object Object]
// example is for person object person.name
// we can use Object.values, for in loop also
// to log the full object
// another way is JSON.stringify(objectName)
// it converts the object as string (typeof shows string)

// interesting this is
// stringify does not convert methods into string
// we should convert it string using toString method
// ex:person.age.toString()

// we can also stringify array
// it looks like array but typeof is string

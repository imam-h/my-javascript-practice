// / General Methods

// Copies properties from a source object to a target object
// Object.assign(target, source)

// Creates an object from an existing object
// Object.create(object)

// Returns an array of the key/value pairs of an object
// Object.entries(object)

// Creates an object from a list of keys/values
// Object.fromEntries()

// Returns an array of the keys of an object
// Object.keys(object)

// Returns an array of the property values of an object
// Object.values(object)

// Groups object elements according to a function
// Object.groupBy(object, callback)

// / Real demonstration

// / JavaScript Object.assign()
// The Object.assign() method copies properties
// from one or more source objects to a target object.

// Create Target Object
const person1 = {
  firstName: "John",
  lastName: "Doe",
  age: 50,
  eyeColor: "blue",
};

// Create Source Object
const person2 = { job: "None", DOB: "1-1970" };

// Assign Source to Target
const joinedObject = Object.assign(person1, person2);

console.log(joinedObject);

// / JavaScript Object.entries()
// ECMAScript 2017 added the Object.entries()
// method to objects.

// Object.entries() returns an array of the
// key/value pairs in an object:
console.log(Object.entries(joinedObject)); // returns an array

// / JavaScript Object.fromEntries()
// The fromEntries() method creates an object
// from a list of key/value pairs.

const fruits = [
  ["apples", 300],
  ["pears", 900],
  ["bananas", 500],
];
console.log(Object.fromEntries(fruits)); // convert array into object

// / JavaScript Object.values()
// Object.values() is similar to
// Object.entries(), but returns a single dimension
// array of the object values:

// joinedObject is in 42 line
console.log(Object.values(joinedObject)); // convert object to array

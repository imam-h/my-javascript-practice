// / All JavaScript objects inherit
// / properties and methods from a prototype.
// we already learned how to use an object constructor
// We also learned that you can not add a new property
// to an existing object constructor
// To add a new property to a constructor,
// you must add it to the constructor function

function Person(first, last, age, eyecolor) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.eyeColor = eyecolor;
}

Person.nationality = "English"; // not added to Person Constructor

// / Prototype Inheritance
// All JavaScript objects inherit properties and
// methods from a prototype:

// Date objects inherit from Date.prototype
// Array objects inherit from Array.prototype
// Person objects inherit from Person.prototype
// The Object.prototype is on the top of the
// prototype inheritance chain

// Date objects, Array objects, and Person objects
// inherit from Object.prototype.

// / Adding Properties and Methods to Objects
// Sometimes you want to add new properties
// (or methods) to all existing objects of a given type.

// Sometimes you want to add new properties
// (or methods) to an object constructor.

// Add a new property to constructor function
Person.prototype.nationality = "English"; // added to Person Constructor

// Add a new property(method) to constructor function
Person.prototype.name = function () {
  return this.firstName + " " + this.lastName;
};

const John = new Person("John", "Doe", 50, "blue");

console.log(John);

console.log(John.nationality); // English
console.log(John.name()); // John Doe

// ! Only modify your own prototypes.
// ! Never modify the prototypes of standard JavaScript objects.

// Example
const x = new String("X");
String.prototype.doingBad = function () {
  return "why";
};

// Don't do this
// it will add the doingBad function
// in all string variables

// Example
let y = "y";
console.log(y.doingBad());

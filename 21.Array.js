const car = ["x", "y", "z"];
// // first method to create array

const alphabet = [];
alphabet[0] = "a";
alphabet[2] = "b";
console.log(alphabet[1]);
// // second method to create array

console.log(typeof alphabet);
// // arrays are objects in JS

const anArray = [
  "String",
  1,
  {
    object: "Hmm",
  },
  ["arrayInsideTheArray"],
];

console.log(anArray[2]);
console.log(anArray[3]);

// //JavaScript variables can be objects.
// //Arrays are special kinds of objects.
// //for this we can have variables of different types in the same Array.
// //we can have objects, functions, arrays in an Array:

const fruits = ["Banana", "Orange", "Apple", "Mango"];

const fruitsLength = fruits.length;

for (let i = 0; i < fruitsLength; i++) {
  console.log(`In index ${i} is ${fruits[i]}`);
}

// // for loop  -> best to loop through an array
// // but need to write extra code like i , array length

function usedForForEach(singleElement) {
  console.log(singleElement);
}

fruits.forEach(usedForForEach);

// // overcome the for loops as no need to use index
// // litle wierd because forEach doesn't use parameter

const addAtLast = ["a", "b"];
addAtLast[addAtLast.length] = "c";
console.log(addAtLast);

//// easily add at last index

const person = [];
person["firstName"] = "John";
person["lastName"] = "Doe";
person["age"] = 46;
person.length; // Will return 0
person[0]; // Will return undefined

// //Arrays with named indexes are called associative arrays (or hashes).

// //JavaScript does not support arrays with named indexes.

// //In JavaScript, arrays always use numbered indexes.
// //If we use named indexes,
// //JavaScript will redefine the array to an object.
// //After that, some array
// //methods and properties will produce incorrect results.

// !In JavaScript, arrays use numbered indexes.

// !In JavaScript, objects use named indexes.

// !Arrays are a special kind of objects, with numbered indexes.

const points = [40];
// //Create an array with one element:
const pointss = new Array(40);
// // Create an array with 40 undefined elements:

Array.isArray(points);
points instanceof Array;
// //to Recognize an Array
// //both returns true
// // it an array found

let test = [1, 2, 3];
console.log(test.indexOf(2));

// / index all works for Arrays

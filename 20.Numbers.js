//! Only the exceptional things will be added
//! Others are like any other Programming languages

// //Extra large or extra small numbers
// //can be written with scientific (exponent) notation:

let x = 123e5; // 12300000
let y = 123e-5; // 0.00123

// //Integers (numbers without a period or exponent notation)
// //are accurate up to 15 digits
// //The maximum number of decimals is 17

let x1 = 999999999999999; // x will be 999999999999999
let y1 = 9999999999999999; // y will be 10000000000000000, limit crossed

// //Floating point arithmetic is not always 100% accurate:

let x3 = 0.2 + 0.1;

// //To solve the problem above, it helps to multiply and divide:

let x4 = (0.2 * 10 + 0.1 * 10) / 10;

//! WARNING !!

//! JavaScript uses the + operator for both addition and concatenation.

//! Numbers are added. Strings are concatenated.

//! Javascript prioratize string

let one = 10;
let two = "20";
let three = 30;

console.log(one + three + two); // 4020
console.log(two + three + one); // 203010
console.log(one + two + three); // 102030

let a = 10;
let b = "20";

console.log(a / b); // 0.5
console.log(a * b); // 200
console.log(a - b); // -10
console.log(a + b); // 1020

// //JavaScript will try to convert
// //strings to numbers in all numeric operations
// //Except addition

console.log(100 / "abd"); // NaN

// // NaN is a number: typeof NaN returns number
// //NaN is a JavaScript reserved word indicating
// //that a number is not a legal number.
// //Trying to do arithmetic with
// //a non-numeric string will result in NaN
// //Watch out for NaN. If you use NaN in a mathematical operation,
// //the result will also be NaN

p = "a";
console.log(isNaN(p));
// //use the global JavaScript function isNaN()
// //to find out if a value is a not a number

let myNumber = 32;
myNumber.toString(32); // 10
myNumber.toString(16); // hexadecimal
myNumber.toString(12); // duodecimal
myNumber.toString(10); // decimal
myNumber.toString(8); // octal
myNumber.toString(2); // binary

// // By default, JavaScript displays numbers as base 10 decimals.
// //We can use the toString() method to
// //output numbers from base 2 to base 36.

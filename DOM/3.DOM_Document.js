// if we see the document object in browser
// we see it as an different style unlike
// regular object, JS treats it as a special type
// of object, but definitely an object
// in the document object, we get properties and methods

// The document object represents your web page.

// If you want to access any element in an HTML page,
// you always start with accessing the document object.

// Below are some examples of how you can use
// the document object to access and manipulate HTML.

// / Finding HTML Elements

//             Method	                         Description
// document.getElementById("id")	         Find an element by element id
// document.getElementsByTagName("name")     Find elements by tag name
// document.getElementsByClassName("name")   Find elements by class name

// / Changing HTML Elements
//                 Property	                    Description
// element.innerHTML = new html content	    Change the inner HTML of an element
// element.attribute = new value	        Change the attribute value of an HTML element
// element.style.property = new style	    Change the style of an HTML element

// / And there is
// / Adding and Deleting Elements
// / Adding Events Handlers
// / Finding HTML Objects

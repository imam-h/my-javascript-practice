// / Finding HTML Elements
// Often, with JavaScript,
// you want to manipulate HTML elements.

// To do so, you have to find the elements first.
// There are several ways to do this:

// Finding HTML elements by id
// Finding HTML elements by tag name
// Finding HTML elements by class name
// Finding HTML elements by CSS selectors
// Finding HTML elements by HTML object collections

// / Finding HTML Element by Id

// This example finds the element with id="intro":

// const element = document.getElementById("intro");

// / Finding HTML Elements by Tag Name

// This example finds all <p> elements:
// const element = document.getElementsByTagName("p");

// This example finds the element with id="main",
// and then finds all <p> elements inside "main":

// const x = document.getElementById("main");
// const y = x.getElementsByTagName("p");

// / Finding HTML Elements by Class Name
// If you want to find all HTML elements with
// the same class name, use getElementsByClassName().

// const x = document.getElementsByClassName("intro");

// / Finding HTML Elements by CSS Selectors
// If you want to find all HTML elements that
// match a specified CSS selector
// (id, class names, types, attributes, values of attributes, etc), use the querySelectorAll() method.

// This example returns a list of all <p> elements with class="intro".

// const x = document.querySelectorAll("p.intro");

// The following HTML objects (and object collections) are also accessible:

// document.anchors // get all the anchor tags
// document.body
// document.documentElement
// document.embeds
// document.forms
// document.head
// document.images // get all the image tag's elements
// document.links
// document.scripts
// document.title

// / Changing HTML Content
document.getElementById("p1").innerHTML = "New text!";

// / Example explained:

// The HTML document above contains
// a <p> element with id="p1"
// We use the HTML DOM to get
// the element with id="p1"
// A JavaScript changes the content (innerHTML)
// of that element to "New text!"

// / Changing the Value of an Attribute
document.getElementById("myImage").src = "landscape.jpg";

// / Example explained:
// The HTML document above contains
// an <img> element with id="myImage"
// We use the HTML DOM to get
// the element with id="myImage"
// A JavaScript changes the src attribute
// of that element from "smiley.gif" to "landscape.jpg"

// /Dynamic HTML content
// JavaScript can create dynamic HTML content
document.getElementById("demo").innerHTML = "Date : " + Date();

// /document.write()
// In JavaScript, document.write()
// can be used to write directly to the HTML output stream:
// ! Never use document.write() after the document is loaded.
// !It will overwrite the document.

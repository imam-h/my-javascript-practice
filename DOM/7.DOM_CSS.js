// / now we want to change the
// / css property via Javascript

const p = document.querySelector("#demo");

// / if we wanted to use getElementById method
// const p = document.getElementById("demo");

// / changing CSS properties
p.style.color = "red";

// ! careful!!! fontSize not font-size
p.style.fontSize = "50px";

const date = new Date();
console.log(date.getFullYear());
console.log(date.getTime());
console.log(date.getMonth());

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const d = new Date();
let month = months[d.getMonth()];
console.log(month);

// /jUST READ documents from w3school
// /Date methods change time to time
// /and easy to understand
// /UTC methods use UTC time (Coordinated Universal Time).

/// UTC time is the same as GMT (Greenwich Mean Time).

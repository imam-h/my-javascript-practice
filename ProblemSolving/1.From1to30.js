// / Q::How to print from 1 to 6 (both inclusive)

function getRandIntegerEx(max, min) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

console.log(getRandIntegerEx(1, 6));

// / Q::Print class students name alphabetically

const students = ["Appel", "Sobuj", "Sayeed"];
console.log(students.sort());

// /sort method also changes the actual array

///Q::Print class students roll in ascending order
const roll_numbers = [7, 6, 4, 3, 5, 2, 1];

console.log(
  roll_numbers.sort(function (a, b) {
    return a - b;
  })
);
// /sort method also changes the actual array

// /Q:: find if any year is Leap year
function isLeapYear(year) {
  if (year % 4 === 0 && (year % 400 === 0 || year % 100 !== 0)) {
    console.log(`${year} is a leap year`);
  } else {
    console.log(`${year} is not a leap year`);
  }
}

isLeapYear(2100);

///  If a year is divisible by 4, it's a potential leap year.
/// However, if the year is divisible by 100, it's not a leap year unless the year is divisible by 400.

// /Q:: Find how many vowels are there in the sentence
const vowels = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"];

function countVowels(sentence) {
  let count = 0;
  const letters = Array.from(sentence);

  letters.forEach(function (value) {
    if (vowels.includes(value)) {
      count++;
    }
  });
  return count;
}
console.log(countVowels("Type something"));

// /Q:: Find how to extract duplicate elements from an array
const numbers = [1, 2, 4, 4, 5, 6, 7, 8, 8, 9, 6];

const dupliactes = numbers.filter(function (value, index, array) {
  return array.indexOf(value) !== index;
});

console.log(dupliactes);

// / indexof(value)       index
// /      index of 1 is 0           0
// /      index of 2 is 1           1
// /      index of 4 is 2           2
// /      index of 4(2nd) is 2 =!   3
// /      index of 5 is 4           4
// /      index of 6 is 5           5
// /      index of 7 is 6           6
// /      index of 8 is 7           7
// /      index of 8(2nd) is 7 =!   8
// /      index of 9 is 9           9
// /      index of 6(2nd) is 5 =!   10
// / The du

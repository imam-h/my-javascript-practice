// / 1st problem
// / Find the string occurences (how many times)
let sentence =
  "Learn with Sumit is all about teaching web development skills and techniques in an efficient and practical manner. Sumit has a extra ordinary learing skill";

let matches = sentence.match(/sumit/gi);
// match() returns an array
// containing the match string ex ['sumit','sumit']

let occurences = matches ? matches.length : "No match";
// if we don't give condition
// will return an error
// because null can' have length property

console.log(occurences);

// if there is no match,
// we get null/"not match" in this case,
// not empty array
// so not cause any problem/error

// / 2nd problem
// / Find the string matched position (first occurence index)
let position = sentence.search(/sumit/i);
position = position >= 0 ? position : "Not found";
// search() retruns the first
// occurence index

console.log(position);

// / 3rd problem
// / Binary search
// / Return the first index of the match
// / where an array and and search value are given
// / as parameters

function linerSearch(array, val) {
  let length = array.length;
  for (i = 0; i < length; i++) {
    if (array[i] === val) {
      return i + 1; // normal position, not engineers :)
    }
  }
  return "Not Found";
}

console.log(linerSearch(["a", "b", "c"], "a"));

// / 4th problem
// / find the largest string from an array
// / and the index too
function longestStringinArray(names) {
  let longestWord = "";
  for (name of names) {
    if (name.length > longestWord.length) {
      longestWord = name;
    }
  }
  return [longestWord, names.indexOf(longestWord)];
}

console.log(longestStringinArray(["Hi there", "Hello world", "Javascript"]));

// / 5th problem
// / from 1-100, find
// / which numbers are divisible by 3 or 5
// / and both 3 and 5

function fizzBuzz(number) {
  for (let i = 1; i <= number; i++) {
    if (i % 15 === 0) {
      console.log(`${i} is a FizzBuzz`);
    } else if (i % 3 === 0) {
      console.log(`${i} is a Fizz`);
    } else if (i % 5 === 0) {
      console.log(`${i} is a Buzz`);
    } else {
      console.log(i);
    }
  }
}

fizzBuzz(100);

// / 6th problem
// / remove the flasy values
// / from an array

// ! Javascript 6 falsies are
// ? false, undefined, null, '', 0, NaN

const mixedArr = [
  "lws",
  "",
  undefined,
  NaN,
  false,
  "apple",
  "Thanks",
  "No value",
];

const trueArray = mixedArr.filter(function (eliminate) {
  if (eliminate) {
    return true;
  } else {
    return false;
  }
});

// / we can short written the code
// const trueArray = mixedArr.filter(Boolean);

console.log(trueArray);

// / 7th problem
// / remove the falsy
// / values from an object

const falsyObject = {
  a: "present",
  b: "",
  c: undefined,
  d: false,
  e: null,
  f: 0,
  g: NaN,
};

const truthyObject = function (obj) {
  for (let i in obj) {
    if (!obj[i]) {
      delete obj[i];
    }
  }
  return obj;
};

console.log(truthyObject(falsyObject));

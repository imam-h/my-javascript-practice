/// The break statement "jumps out" of a loop.

/// The continue statement "jumps over" one iteration in the loop.

for (let i = 0; i < 10; i++) {
  if (i === 3) {
    break;
  }
  console.log(i);
}

for (let i = 0; i < 10; i++) {
  if (i === 3) {
    continue;
  }
  console.log(i);
}

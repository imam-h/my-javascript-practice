// /In JavaScript, the this keyword refers to an object.

// /The this keyword refers to different objects depending on how it is used:

// /In an object method, this refers to the object.

// /           Alone, this refers to the global object.

let x = this;
console.log(x);

// /           In a function, this refers to the global object.

function myFunction() {
  return this;
}
console.log(myFunction());

// /           In a function, in strict mode, this is undefined.
// "use strict";
// let x = this;
// console.log(x);

// /           In an event, this refers to the element that received the event.
// In HTML event handlers,
// this refers to the HTML element that received the event:

// /Methods like call(), apply(), and bind() can refer this to any object.

const person1 = {
  fullName: function () {
    return this.firstName + " " + this.lastName;
  },
};

const person2 = {
  firstName: "John",
  lastName: "Doe",
};

// Return "John Doe":
console.log(person1.fullName.call(person2));

// /The example above calls person1.fullName
// /with person2 as an argument,
// /this refers to person2,
// /even if fullName is a method of person1
// /call() function invoked when we call a function
// / no need to use explicitly for normal use

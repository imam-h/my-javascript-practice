// The "use strict" directive was
// new in ECMAScript version 5.

// It is not a statement,
// but a literal expression,
// ignored by earlier versions of JavaScript.

// The purpose of "use strict" is to
// indicate that the code should be executed in "strict mode".

// With strict mode, you can not,
// for example, use undeclared variables.

"use strict";
x = 44;
/// error, x not defined
// / without "use strict";
// / won't get the error

myFunction();

function myFunction() {
  y = 3.14; // This will also cause an error because y is not declared
}

function myFunction1() {
  "use strict";
  y = 3.14; // This will cause an error
}

myFunction1();

// /                Why Strict Mode?
// / Strict mode makes it easier to write "secure" JavaScript.

// / Strict mode changes previously accepted "bad syntax" into real errors.

// /          Not Allowed in Strict Mode
// / There are a whole list , see w3school website

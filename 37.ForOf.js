// /for of statement loops through the values of an iterable object.

// /It lets you loop over iterable data

// /structures such as Arrays, Strings, Maps, NodeLists, and more

// / Simply
// / For objects use for in loop (as it gives index)
// / For others (iterable) use for of loop

const cars = ["BMW", "Volvo", "Mini"];
const numbers = [1, 2, 3];

for (let x of cars) {
  console.log(x);
}
for (let number of numbers) {
  console.log(number);
}

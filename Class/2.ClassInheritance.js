class Car {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return "I have a " + this.carname;
  }
}

class Model extends Car {
  constructor(brand, mod) {
    super(brand);
    this.model = mod;
  }
  show() {
    return this.present() + ", it is a " + this.model;
  }
}

let myCar = new Model("Ford", "Mustang");

// /Inheritance is useful for code reusability: reuse properties and methods of
// /an existing class when you create a new class.

// / Getters and Setters
// Classes also allows you to use getters
// and setters.
// It can be smart to use getters and
// setters for your properties, especially if you want to do something special with the value before returning them, or before you set them.

// To add getters and setters in the class,
// use the get and set keywords.

class Carr {
  constructor(brand) {
    this.carname = brand;
  }
  get cnam() {
    return this.carname;
  }
  set cnam(x) {
    this.carname = x;
  }
}

const firstCar = new Carr("Ford");

console.log(firstCar.cnam);

firstCar.cnam = "fd";
console.log(firstCar.cnam);

// ! even if the getter is a method, you do not use parentheses
// ! when you want to get the property value.

// The name of the getter/setter method cannot be the
// same as the name of the property, in this case carname.

// Many programmers use an underscore character
// _ before the property name to separate
// the getter/setter from the actual property:

// constructor(brand) {
//   this._carname = brand;
// }

// /Hoisting
// Unlike functions, and other
// JavaScript declarations, class declarations are not hoisted.

// That means that you must declare
// a class before you can use it

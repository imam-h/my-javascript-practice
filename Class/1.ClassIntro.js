// ECMAScript 2015, also known as ES6, introduced JavaScript Classes.

// JavaScript Classes are templates for JavaScript Objects.

// / JavaScript Class Syntax
// Use the keyword class to create a class.

// Always add a method named constructor():

class Car {
  constructor(name, year) {
    this.brand = name;
    this.release_year = year;
  }
}
const firstCar = new Car("BMW", 1222);
console.log(firstCar);

// A JavaScript class is not an object.

// It is a template for JavaScript objects.
// just like function

// / The Constructor Method
// The constructor method is a special method:

// It has to have the exact name "constructor"
// It is executed automatically when a new object is created
// It is used to initialize object properties
// ! If you do not define a constructor method, JavaScript will add an empty constructor method.

// /Class Methods
// Class methods are created with the
// same syntax as object methods.

class Bike {
  constructor(name, year) {
    this.name = name;
    this.year = year;
  }
  age() {
    console.log(`Using ${this.name} for 2 years`);
  }
}

const yamaha = new Bike("Yamaha", 1999);
yamaha.age();

// / You can send parameters to Class methods

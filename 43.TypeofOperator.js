// /JavaScript has 7 primitive data types:

/// string
/// number
/// boolean
/// bigint
/// symbol
/// null
/// undefined

/// JavaScript has one complex data type:

//!  object

/// All other complex types like arrays,
/// functions, sets, and maps are just different types of objects

/// The typeof operator returns only two types in non-primitive case:

///! object
///! function

console.log(typeof typeof 2); /// returns string

/// How to Recognize an Array
const fruits = ["apples", "bananas", "oranges"];
Array.isArray(fruits); ///returns true

// /The instanceof Operator

const time = new Date();
console.log(time instanceof Date); ///returns true

const fruitsMap = new Map([
  ["apples", 500],
  ["bananas", 300],
  ["oranges", 200],
]);

console.log(
  fruitsMap instanceof Map ///returns true
);

// /Undefined Variables
// /The typeof of an undefined variable is undefined

let car = "";

console.log(typeof car);

car = undefined; //just let car also means undefined

console.log(typeof car);

// /Any variable can be emptied,
// /by setting the value to undefined.
/// The type will also be undefined.

// /Null
// /In JavaScript null is "nothing". It is supposed to be something that doesn't exist.

// /Unfortunately, in JavaScript, the data type of null is an object.

// /You can empty an object by setting it to null

// /You can also empty an object by setting it to undefined or null

console.log(typeof null); /// object

/// Difference Between Undefined and Null
/// undefined and null are equal in value but different in type

typeof undefined; // undefined
typeof null; // object

null === undefined; // false
null == undefined; // true

// /The constructor Property
// /The constructor property returns the constructor
// /function for all JavaScript variables

// Returns function Object() {[native code]}:
let x = { name: "John", age: 34 }.constructor;
console.log(x);

// Returns function Array() {[native code]}:
let y = [1, 2, 3, 4].constructor;
console.log(y);

// Returns function Date() {[native code]}:
let d = new Date().constructor;
console.log(d);

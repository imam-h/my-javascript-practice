// / Use if to specify a block of code to be executed, if a specified condition is true
/// Use else to specify a block of code to be executed, if the same condition is false
/// Use else if to specify a new condition to test, if the first condition is false

if (condition1) {
  //  block of code to be executed if condition1 is true
} else if (condition2) {
  //  block of code to be executed if the condition1 is false and condition2 is true
} else {
  //  block of code to be executed if the condition1 is false and condition2 is false
}

/// Use switch to specify many alternative blocks of code to be executed
/// Switch cases use strict comparison (===).

switch (new Date().getDay()) {
  case 0:
    day = "Sunday";
    break;
  case 1:
    day = "Monday";
    break;
  default:
    text = "No day found";
  case 2:
    day = "Tuesday";
    break;
  case 3:
    day = "Wednesday";
    break;
  case 4:
    day = "Thursday";
    break;
  case 5:
    day = "Friday";
    break;
  case 6:
    day = "Saturday";
}
console.log(day);

// /When JavaScript reaches a break keyword,
// /it breaks out of the switch block.
// /This will stop the execution inside the switch block.
// /It is not necessary to break the last case in a switch block. The block breaks (ends) there anyway.
// /The default keyword specifies the code to run
// /if there is no case match:
/// The default case does not have to be the last case in a switch block

switch (new Date().getDay()) {
  case 4:
  case 5:
    text = "Soon it is Weekend";
    break;
  case 0:
  case 6:
    text = "It is Weekend";
    break;
  default:
    text = "Looking forward to the Weekend";
}

// /In this example case 4 and 5 share the same code block,
// /and 0 and 6 share another code block

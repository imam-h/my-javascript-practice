const cars = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];

for (let index = 0; index < cars.length; index++) {
  const element = cars[index];
  console.log(`${element} at position ${index + 1}`);

  ///   return ; // stop loop after first iteration
}

for (i = 0, j = 0, k = 0; i < cars.length; i++, j++, k++) {
  console.log(i * j * k);
}

/// Expression 1 sets a variable before
// /the loop starts (let i = 0 & let j=0).
// /separated by comma

/// Expression 2 defines the condition for the loop to run

/// Expression 3 increases a value (i++ and j ++)
// /each time the code block in the loop has been executed.

let f = 0;
let len = cars.length;
let text = "";
for (; f < len; f++) {
  console.log(cars[f]);
}

// /you can omit expression 1
// /(like when your values are set before the loop starts):

// /you can omit expression 2 also
// /in this case you should use break
// /to stop the loop

// /Don't decalre variables using var
// /If you do, and use the variable in for loop
// /it will change the variable value

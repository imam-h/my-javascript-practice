// /Unlike other objects, the Math object has no constructor.

// /The Math object is static.

/// All methods and properties can be used without creating a Math object first.

console.log(Math.PI);

// /JavaScript provides 8 mathematical constants
// /that can be accessed as Math properties
// /PI is an example

console.log("round");

console.log(Math.round(4.9)); // return 5
console.log(Math.round(4.4)); // return 4
console.log(Math.round(-4.4)); // return -4

console.log("ceil");

console.log(Math.ceil(4.9));
console.log(Math.ceil(4.2));
console.log(Math.ceil(-4.2));

console.log("floor");
console.log(Math.floor(4.9));
console.log(Math.floor(4.2));
console.log(Math.floor(-4.2));

console.log("trunc");
console.log(Math.trunc(4.9));
console.log(Math.trunc(4.2));
console.log(Math.trunc(-4.2));

/// Math.round(x)	Returns x rounded to its nearest integer
/// Math.ceil(x)	Returns x rounded up to its nearest integer
/// Math.floor(x)	Returns x rounded down to its nearest integer
/// Math.trunc(x)	Returns the integer part of x (new in ES6)

// /4 common methods to round a number to an integer

console.log(Math.sign(-4)); // return -1
console.log(Math.sign(0)); // returns 0
console.log(Math.sign(4)); // returns 1

///Math.sign(x) returns if x is negative,
///null or positive:

console.log("Trigonometry");

console.log(Math.sin((30 * Math.PI) / 180));
/// into the radian value with PI/180

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
// /This JavaScript function always returns a
// /random number between min (included) and max (excluded):

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/// This JavaScript function always returns
// /a random number between min and max (both included):

// //       Before Arrow:
// const hello = function() {
//   return "Hello World!";
// }

// //       With Arrow Function:
let hello = () => {
  return "Hello World!";
};

// /  we can make it even more shorter
// / This works only if the function has only one statement.

hello = () => "Hello World with short syntax!";

console.log(hello());

// /     Arrow Function With Parameters:
//      /hello = (val) => "Hello " + val;

// /In fact, if you have only one parameter,
// /you can skip the parentheses as well:

// /     Arrow Function Without Parentheses:
//      /hello = (val) => "Hello " + val;

//                /In short,
// /with arrow functions there are no binding of this.

// /In regular functions the this keyword
// /represented the object that called the function, which could be the window, the document, a button or whatever.

// /With arrow functions the this
// /keyword always represents the object that defined the arrow function.

const indexOfLocate = "Index Of AKA Locate index";

console.log(indexOfLocate.indexOf("A"));
// // The indexOf() method returns the index (position) of the first occurrence
// // of a string in a string, or it returns -1 if the string is not found

console.log(indexOfLocate.lastIndexOf("A"));
// // The lastIndexOf() method returns the index of the last occurrence
// // of a specified text in a string
// // Both methods accept a second parameter as the starting position for the search

console.log(indexOfLocate.search("A"));
// //The search() method searches a string for a string
// // (or a regular expression) and returns the position of the match

// ! The two methods are NOT equal. These are the differences:

// ! The search() method cannot take a second start position argument.
// ! The indexOf() method cannot take powerful search values (regular expressions)

console.log(indexOfLocate.match("dex"));
// //The match() method returns an array containing
// //the results of matching a string against a string (or a regular expression).

console.log(indexOfLocate.includes("dex", 22));
// //The includes() method returns true
// //if a string contains a specified value

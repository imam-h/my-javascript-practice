// /for in - loops through the properties of an Object
const person = { fname: "John", lname: "Doe", age: 25 };

for (let x in person) {
  console.log(x, person[x]);
}

// /In array
// /Best practise not to use
// //for in arrays, only for objects
// /because order is not important in object,
// /but important in arrays
const numbers = [45, 4, 9, 16, 25];
for (let number in numbers) {
  console.log(numbers[number]);
}

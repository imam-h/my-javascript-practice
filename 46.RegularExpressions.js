// /A regular expression is a sequence of
// /characters that forms a search pattern.

// /The search pattern can be used
// /for text search and text replace operations.

// /Using String Methods
// /In JavaScript, regular expressions are often used
// /with the two string methods: search() and replace().

let text = "Visit W3Schools! W3Schools";

// /The search() method uses an expression
// /to search for a match, and returns the position of the match.

console.log(text.search("W3Schools "));

// /The replace() method returns a modified string
// /where the pattern is replaced.

let result = text.replace("W3Schools", "w3schools");
console.log(result); /// Visit W3Schools!

// /or
// /let result = text.replace(/microsoft/i, "W3Schools");

// /i	Perform case-insensitive matching
console.log(text.match(/w3schools/i)); /// i = insensitive

/// g	Perform a global match (find all)
console.log(text.match(/w3schools/g)); /// i = insensitive

// m	Perform multiline matching
// d    Perform start and end matching

// / there is many pattern to learn
// / by practice

console.log(/e/.test("The best things in life are free!"));
console.log(/e/.exec("The best things in life are free!"));

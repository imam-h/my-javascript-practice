// /A JavaScript Set is a collection of unique values.
// /Each value can only occur once in a Set.
/// The values can be of any type,
// /primitive values or objects.

// Create a Set
const letters = new Set(["a", "b", "c"]);

// Add Values to the Set
letters.add("d");

console.log(letters);

// Create a Set
const anotherLetters = new Set();

// Create Variables
const a = "a";
const b = "b";
const c = "c";

// Add Variables to the Set
// dupilcates won't be added
anotherLetters.add(a);
anotherLetters.add(b);
anotherLetters.add(c);
anotherLetters.add(c);

console.log(anotherLetters);

// List all entries
letters.forEach(function (value, index, array) {
  console.log(value);
});

// /This is not the actual forEach for arrays
// /we can only print the value (not index and array)

// /Example down here
const x = ["w"];
x.forEach(function (value, index, array) {
  console.log(value);
  console.log(index);
  console.log(typeof index);
  console.log(array);
});

// /The values() method returns
// /an Iterator object with the values in a Set

console.log(letters.values());

for (const entry of letters.values()) {
  console.log(entry);
}

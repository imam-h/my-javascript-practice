// /A Map holds key-value pairs
// /where the keys can be any datatype.
// !But in object data type
// !the key must be string
// /A Map remembers the original insertion order of the keys.

// Create a Map
const fruits = new Map([
  ["apples", 500],
  ["bananas", 300],
  ["oranges", 200],
]);

// ! Or the below way
// Create a Map
// const fruits = new Map();

// Set Map Values
// fruits.set("apples", 500);
// fruits.set("bananas", 300);
// fruits.set("oranges", 200);

// Set Map Values
fruits.set("Berry", 200);

// Get Map Values
console.log(fruits.get("apples"));

// the size of the map
console.log(fruits.size);

// Delete from map
fruits.delete("apples");

console.log(fruits);

// removes all the elements from a map
fruits.clear();

// /has() method returns true if a key exists in a map
// /fruits.has("apples");

// ! Notice
/// map also has
/// forEach(),
/// keys()
/// values()
/// etc. methods

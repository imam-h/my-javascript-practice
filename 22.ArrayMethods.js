let array = ["c", "o", "d", "e"];

console.log(array.toString());

console.log(array.join(" * "));

// /toString() converts an array to
// /a string of (comma separated) array values.(By default)
// /join() method also joins all array elements into a string
// /Better is join()

const fruits = ["Banana", "Orange", "Apple", "Mango"];

console.log(fruits.pop());

console.log(fruits.push("Kiwi"));

console.log(fruits.shift());

console.log(fruits.unshift("Berry"));

console.log(fruits);

console.log(fruits.at(2));

// /ES2022 intoduced the array method at()

//! (Heading )Changing Elements

const car = ["Rickshaw", "Van", "Trolly", "Truck"];

car[0] = "Auto";

car[car.length] = "Rickshaw";

delete car[0];

// /Using delete() leaves undefined holes in the array.

/// Use pop() or shift() instead.

car.splice(0, 1, "CNG");

// / numeber not as index, how many to replace, what we want to add as as string)
// /ES2023 added the Array toSpliced() method as a safe way
// /to splice an array without altering the original array.

console.log(car);

const carWithFruits = fruits.concat(car);

/// The concat() method does not change the existing arrays.
// /It always returns a new array.
// /The concat() method can take any number of array arguments.

console.log(carWithFruits.concat("Cycle"));

console.log(carWithFruits);

///prints without Cycle

const anotherFruits = ["Banana", "Orange", "Apple", "Mango"];

const sliced = anotherFruits.slice(0, 1);

console.log(sliced);

console.log(anotherFruits);

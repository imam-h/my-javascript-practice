// ! All string methods return a new string. They don't modify the original string.

// ! Formally said:

// ! Strings are immutable: Strings cannot be changed, only replaced.

const sliceExample = "Development";

const positiveIndex = sliceExample.slice(2, 8);
const minusIndex = sliceExample.slice(-7, -2);

console.log(positiveIndex);
console.log(minusIndex);

console.log(typeof positiveIndex);

// // slice() extracts a part of a string
// // and returns the extracted part in a new string.
// // The method takes 2 parameters: first > second (-) sign neglected
// // start position, and end position (end not included)
console.log("***************sliceExample Ends*******************");

const subStringExample = "SUBSTRING";

console.log(subStringExample.substring(0, 3));

console.log(subStringExample.substring(1));

// // substring() is similar to slice() but avoid negative params.
// // The difference is that
// // start and end values less than
// // 0 are treated as 0 in substring().
console.log("***************subStringExample Ends*******************");

const subSTRExample = "SUBSTR";

console.log(subSTRExample.substr(3));

console.log(subSTRExample.substr(-5, 4));

// // substr() is similar to slice().
// // The difference is that
// // the second parameter specifies
// // the length of the extracted part.
// // including the first index char
// // same as negative index
console.log("*************** subSTRExample Ends *******************");

const replace = "Javascript";

console.log(replace.replace("Java", "Khaba"));

// // The replace() method returns a new string.
// // The replace() method replaces only the first match
// // By default, the replace() method is case sensitive.
// // Writing MICROSOFT (with upper-case) will not work:
console.log("*************** replace Ends *******************");

const lowerCase = "abcd";
const upperCase = "ABCD";

console.log(lowerCase.toUpperCase());
console.log(upperCase.toLowerCase());

// //A string is converted to upper case with toUpperCase():
// // A string is converted to lower case with toLowerCase():
console.log("*************** upper lower Case Ends *******************");

const first = "Java";
const second = "Script";

console.log(first.concat(second));

console.log(first.concat(" ", second));
// // concat() joins two or more strings:
console.log("*************** concat Ends *******************");

const trim = "      what?   3   ";

const trimToo = "      what?      ";

console.log(trim.trim());

console.log(trimToo.trim());

// // The trim() method removes whitespace
// // from both sides of a string:
console.log("*************** trim Ends *******************");

let find = "FOUND";
console.log(find.charAt(2));
console.log(find.charCodeAt(2));

// // The charAt() method returns the character at a
// // specified index (position) in a string
// // charCodeAt() method returns a UTF-16 code (an integer between 0 and 65535).
console.log("*************** CharAt etc Ends *******************");

const split = "MakemeArray whate youWant";
console.log(split.split(" "));
console.log(split.split(""));
console.log(split.split(","));
// // A string can be converted to an array with the split() method
// // If the separator is omitted, the returned array will
// //contain the whole string in index [0].
// //If the separator is "",
// // the returned array will be an array of single characters

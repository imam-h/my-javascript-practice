const paymentSuccess = true;
const marks = 90;

function enroll() {
  console.log("Course enrollment is in progress");

  const promise = new Promise(function (resolve, reject) {
    setTimeout(() => {
      if (paymentSuccess) {
        resolve();
      } else {
        reject("Payment failed");
      }
    }, 2000);
  });

  return promise;
}

function progress() {
  console.log("Course on Progress ....");

  const promise = new Promise(function (resolve, reject) {
    setTimeout(() => {
      if (marks >= 80) {
        resolve();
      } else {
        reject("Not enough marks");
      }
    }, 2000);
  });
  return promise;
}

function getCertificate() {
  console.log("Preparing the certificate");

  const promise = new Promise(function (resolve) {
    setTimeout(() => {
      resolve("Congrats for the certificate");
    }, 1000);
  });

  return promise;
}

enroll()
  .then(progress)
  .then(getCertificate)
  .then((value) => {
    console.log(value);
  })
  .catch((err) => {
    console.log(err);
  });

// "I will call back later!"

// / A callback is a function passed
// as an argument to another function

// / This technique allows a function
// to call another function

// / A callback function can run after
// another function has finished

// / JavaScript functions are executed in the sequence they are called.
// Not in the sequence they are defined.

// ! This example will end up displaying "Hello":

function myFirst() {
  console.log("Hello");
}

function mySecond() {
  console.log("Goodbye");
}

mySecond();
myFirst();

// /Sequence Control
// Sometimes you would like to have
// better control over when to execute a function.

// Suppose you want to do a calculation,
// and then display the result.

function myDisplayer(some) {
  console.log(some);
}

function myCalculator(num1, num2) {
  let sum = num1 + num2;
  return sum;
}

let result = myCalculator(5, 5);
myDisplayer(result);

// The problem with the first example above,
// is that you have to call two functions to display the result.

// The problem with the second example,
// is that you cannot prevent the calculator function from displaying the result.
// ! function myCalculator(num1, num2) {
//   let sum = num1 + num2;
//   myDisplayer(sum);
// }
// myCalculator(4, 5)
// / Now it is time to bring in a callback.

function myCalculatorCallback(num1, num2, callback) {
  let sum = num1 + num2;
  callback(sum);
}
myCalculatorCallback(4, 7, myDisplayer); // a function as parameter

// !When you pass a function as an argument,
// !remember not to use parenthesis.

// / The examples above are not very exciting.

// / They are simplified to teach you the callback syntax.

// / Where callbacks really shine are in asynchronous functions,
// / where one function has to wait for another function

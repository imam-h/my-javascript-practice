// This is a follow up tutorial
// not mentioned as a single topic in w3school

const paymentSuccess = true;
const marks = 70;

function enroll(callback) {
  console.log("enrollment is in progress");

  setTimeout(function () {
    if (paymentSuccess) {
      callback();
    } else {
      console.log("Payment failed");
    }
  }, 3000);
}

function progress(callback) {
  console.log("Course on Progress");

  setTimeout(function () {
    if (marks >= 90) {
      callback();
    } else {
      console.log("Not enough marks");
    }
  }, 2000);
}

function getCertificate() {
  console.log("Preparing the certificate");

  setTimeout(() => {
    console.log("Congrats for the certificate");
  }, 1000);
}

// enroll(progress);
// progress(getCertificate);
// / the above code doesn't give desired result

enroll(() => {
  progress(() => getCertificate);
});
// / but this above one does
// / as parameter taking the whole function

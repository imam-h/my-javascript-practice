// / async and await make promises easier to write"

// / async makes a function return a Promise

// / await makes a function wait for a Promise

// / async/await will make it easier/look easier
// / the same task as we did in Promises & CallbackPattern section

const paymentSuccess = true;
const marks = 90;

function enroll() {
  console.log("Course enrollment is in progress");

  const promise = new Promise(function (resolve, reject) {
    setTimeout(() => {
      if (paymentSuccess) {
        resolve();
      } else {
        reject("Payment failed");
      }
    }, 2000);
  });

  return promise;
}

function progress() {
  console.log("Course on Progress ....");

  const promise = new Promise(function (resolve, reject) {
    setTimeout(() => {
      if (marks >= 80) {
        resolve();
      } else {
        reject("Not enough marks");
      }
    }, 2000);
  });
  return promise;
}

function getCertificate() {
  console.log("Preparing the certificate");

  const promise = new Promise(function (resolve) {
    setTimeout(() => {
      resolve("Congrats for the certificate");
    }, 1000);
  });

  return promise;
}

async function course() {
  try {
    await enroll();
    await progress();
    const message = await getCertificate();
    console.log(message);
  } catch (error) {
    console.log(error);
  }
}

course();

// / Functions running in parallel with
// / other functions are called asynchronous

// / A good example is JavaScript setTimeout(), setInterval()

console.log("Line 1");

setTimeout(() => {
  console.log("In set time out");
}, 2000);

console.log("Line 3");

// / Callback Alternatives
// With asynchronous programming,
// JavaScript programs can start long-running tasks,
// and continue running other tasks in parallel.

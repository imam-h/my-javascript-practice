// Scope determines the accessibility (visibility) of variables.

// JavaScript variables have 3 types of scope:

//!
/// 1.Block scope
/// 2.Function scope
/// 3.Global scope

// / 1

// /ES6 introduced two important new JavaScript keywords:
// / let and const.

// / These two keywords provide Block Scope in JavaScript.

{
  let x = 2;
}
// x can NOT be used here

// / Variables declared with the var
// / keyword can NOT have block scope.

/// Variables declared inside a
// /{ } block can be accessed from outside the block.

//!!!! Local Scope
/// Variables declared within a JavaScript function,
// /are LOCAL to the function

// / 2
// !Function Scope
// /Variables defined inside a function are not
// /accessible (visible) from outside the function.

// /Variables declared with var, let and
// /const are quite similar when declared inside a function.

// / 3
// /Global JavaScript Variables
// /A variable declared outside a function, becomes GLOBAL.

// !Automatically Global
// /If you assign a value to a variable that has not been declared,
// /it will automatically become a GLOBAL variable.

// /This code example will declare a global variable carName,
// /even if the value is assigned inside a function.

myFunction();

// code here can use carName

function myFunction() {
  carName = "Volvo"; /// globl carName as o let/var/const
}

// /Global Variables in HTML
// /With JavaScript, the global scope is the JavaScript environment.

// /In HTML, the global scope is the window object.

// /Global variables defined with the var keyword belong to the window object
// /Global variables defined with the let keyword don't belong to the window object

// The Lifetime of JavaScript Variables
// The lifetime of a JavaScript variable starts
// when it is declared. (Ex:: let c)

// Function (local) variables are deleted
// when the function is completed.

// In a web browser, global variables are deleted
// when you close the browser window (or tab).

// Function Arguments
// Function arguments (parameters) work
// as local variables inside functions.

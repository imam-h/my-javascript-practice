const numbers = [45, 4, 9, 16, 25];

console.log("---------forEach Method---------");

function forForEach(value) {
  console.log(value);
}

numbers.forEach(forForEach);

/// the function takes 3 arguments
// /item value,index,The array itself

console.log("---------map Method---------");

function forMap(value) {
  console.log(value);
}

numbers.map(forMap);
/// the function takes same 3 arguments
// /item value,index,The array itself

console.log("---------filter Method---------");

function forfilter(value) {
  return value > 10;
}

let filtered = numbers.filter(forfilter);

console.log(filtered);

/// the function takes same 3 arguments
// /item value,index,The array itself

console.log("---------Reduce Method---------");

function forReduce(total, value, index) {
  console.log(total);
  console.log(value);
  console.log(index);
  console.log("-----");
  return total + value;
}
console.log(numbers.reduce(forReduce));

/// the function takes same 4 arguments
// / total (the initial value / previously returned value)
// / item value,item index,The array itself
// /reduce() method runs a function
// /on each array element to produce (reduce it to) a single value.

console.log("---------Reduce Right Method---------");

function forReduceRight(total, value, index) {
  console.log(total);
  console.log(value);
  console.log(index);
  console.log("-----");
  return total + value;
}
console.log(numbers.reduceRight(forReduceRight));
// /reduceRight() works from right-to-left in the array

console.log("---------Every Method---------");

function forEvery(value) {
  return value > 10;
}

console.log(numbers.every(forEvery));

// /every() method checks if all array values pass a test.
/// the function takes same 3 arguments
// /item value,index,The array itself

console.log("---------indexOf Method---------");

let position = numbers.indexOf(45) + 1;
console.log(position);

// /removed from w3cSchool
// /check the position of our array

console.log("---------find Method---------");

function forFind(value) {
  return value > 10;
}

console.log(numbers.find(forFind));

///Only returns the first value
// /which meets the condition

console.log(numbers);
///Actual array